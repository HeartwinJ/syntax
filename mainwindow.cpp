#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "functions.h"
#include <QMessageBox>
#include <ctype.h>
#include <QFile>
#include <QTextStream>
#include <ctime>
#include <time.h>

//global variables
bool firsttime = true;
char online[50][25];
char chat_str[100];
char chat_str2[100];

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->stackedWidget->setCurrentIndex(0);
    this->setWindowTitle("Login");
    ofstream fout("\\\\192.168.1.104\\guest\\SyntaX\\accounts.dat",ios::app|ios::binary);

    fout.close();
    system("mkdir C:\\SyntaX\\Downloads");
}

MainWindow::~MainWindow()
{
    char online_temp[25];
    strcpy(online_temp,(char*)ui->lineEdit->text().toStdString().c_str());

    char online[50][25];

    //updating array from file
    ifstream fin2("\\\\192.168.1.104\\guest\\SyntaX\\online.txt", ios::in);

    for (int l = 0; l < 50; l++)
    {
        fin2.read((char*)&online[l], sizeof(online[l]));
    }

    fin2.close();

    //updating finished

    for (int m = 0; m < 50; m++)
    {
        if (strcmp(online_temp, online[m]) == 0)
        {
            for (int n = m; n < 49; n++)
            {
                strcpy(online[n], online[n + 1]);
            }
            break;
        }
    }

    ofstream fout2("\\\\192.168.1.104\\guest\\SyntaX\\online.txt", ios::out);

    for (int o = 0; o < 50; o++)
    {
        fout2.write((char*)&online[o], sizeof(online[o]));
    }

    fout2.close();

    char str999[500];

    time_t t = time(0);   // get time now
    struct tm * now = localtime(&t);

    strcpy(str999,"[");
    strcat(str999,(char*)to_string(now->tm_year + 1900).c_str());
    strcat(str999,"/");
    strcat(str999,(char*)to_string(now->tm_mon + 1).c_str());
    strcat(str999,"/");
    strcat(str999,(char*)to_string(now->tm_mday).c_str());
    strcat(str999,"   ");
    strcat(str999,(char*)to_string(now->tm_hour).c_str());
    strcat(str999,":");
    strcat(str999,(char*)to_string(now->tm_min).c_str());
    strcat(str999,":");
    strcat(str999,(char*)to_string(now->tm_sec).c_str());
    strcat(str999,"]");
    strcat(str999,(char*)ui->lineEdit->text().toStdString().c_str());
    strcat(str999," Logged Out");


    ofstream fout999("\\\\192.168.1.104\\guest\\SyntaX\\log.txt",ios::app);
    fout999<<str999<<"\n";
    fout999.close();

    delete ui;
}

void MainWindow::updateall()
{    
    QFile file999("\\\\192.168.1.104\\guest\\SyntaX\\log.txt");
    file999.open(QFile::ReadOnly | QFile::Text);

    QTextStream in999(&file999);
    QString text999 = in999.readAll();
    ui->textBrowser_2->setText(text999);
    file999.close();

    ui->listWidget_2->clear();

    ifstream fin2("\\\\192.168.1.104\\guest\\SyntaX\\online.txt", ios::in);
    //getting values to the array
    for (int k = 0; k < 50; k++)
    {
        fin2.read((char*)&online[k], sizeof(online[k]));
    }

    fin2.close();
    //extracting one string from the array of strings

    for(int i = 0; i < 50; i++)
    {
        if(isalnum(online[i][0]) != 0 && isalnum(online[i][1]) != 0 && isalnum(online[i][2]) != 0)
        {
            //the string corresponding to the current value of i is a string starting with an alphanumeric char

            ui->listWidget_2->addItem(online[i]);

        }
        //QMessageBox::information(0,"Information","I : "+i);
        //else
            //break;
    }

    QFile file(chat_str);
    file.open(QFile::ReadOnly | QFile::Text);

    QTextStream in(&file);
    QString text = in.readAll();
    ui->textBrowser->setText(text);
    file.close();
}

///
///
///

void MainWindow::on_pushButton_4_clicked()
{
    // Page 2 Logout Button

    ui->lineEdit_2->setText("");

    char str999[500];

    time_t t = time(0);   // get time now
    struct tm * now = localtime(&t);

    strcpy(str999,"[");
    strcat(str999,(char*)to_string(now->tm_year + 1900).c_str());
    strcat(str999,"/");
    strcat(str999,(char*)to_string(now->tm_mon + 1).c_str());
    strcat(str999,"/");
    strcat(str999,(char*)to_string(now->tm_mday).c_str());
    strcat(str999,"   ");
    strcat(str999,(char*)to_string(now->tm_hour).c_str());
    strcat(str999,":");
    strcat(str999,(char*)to_string(now->tm_min).c_str());
    strcat(str999,":");
    strcat(str999,(char*)to_string(now->tm_sec).c_str());
    strcat(str999,"]");
    strcat(str999,(char*)ui->lineEdit->text().toStdString().c_str());
    strcat(str999," Logged Out");


    ofstream fout999("\\\\192.168.1.104\\guest\\SyntaX\\log.txt",ios::app);
    fout999<<str999<<"\n";
    fout999.close();

    //to delete the logged out username from the onine list

    char online_temp[25];
    strcpy(online_temp,(char*)ui->lineEdit->text().toStdString().c_str());

    char online[50][25];


    //updating array from file
    ifstream fin2("\\\\192.168.1.104\\guest\\SyntaX\\online.txt", ios::in);

    for (int l = 0; l < 50; l++)
    {
        fin2.read((char*)&online[l], sizeof(online[l]));
    }

    fin2.close();

    //updating finished

    for (int m = 0; m < 50; m++)
    {
        if (strcmp(online_temp, online[m]) == 0)
        {
            for (int n = m; n < 49; n++)
            {
                strcpy(online[n], online[n + 1]);
            }
            break;
        }
    }

    ofstream fout2("\\\\192.168.1.104\\guest\\SyntaX\\online.txt", ios::out);

    for (int o = 0; o < 50; o++)
    {
        fout2.write((char*)&online[o], sizeof(online[o]));
    }

    fout2.close();

    ui->listWidget_2->clear();

    ui->stackedWidget->setCurrentIndex(0);
    QMessageBox::information(0,"Information","Logout Successful!");
}

void MainWindow::on_pushButton_clicked()
{
    //Page 1 Login Button

    MyClass mc;

    int temp;
    temp = mc.validate((char*)ui->lineEdit->text().toStdString().c_str(),(char*)ui->lineEdit_2->text().toStdString().c_str(),1);

    char online_temp[25];
    strcpy(online_temp,(char*)ui->lineEdit->text().toStdString().c_str());

    char online[50][25];
    online[0][0] = '*';

    if(temp == 2)
    {
        //Invalid Username
        QMessageBox::warning(0,"Error","Invalid Username");
    }
    else if(temp == 3)
    {
        //Password Incorrect
        QMessageBox::warning(0,"Error","Incorrect Password");
    }
    else if(temp == 4)
    {
        //All OK
        QMessageBox::information(0,"Information","Successful Login");

        char str999[500];

        time_t t = time(0);   // get time now
        struct tm * now = localtime(&t);

        strcpy(str999,"[");

        strcat(str999,(char*)to_string(now->tm_year + 1900).c_str());

        strcat(str999,"/");

        strcat(str999,(char*)to_string(now->tm_mon + 1).c_str());
        strcat(str999,"/");
        strcat(str999,(char*)to_string(now->tm_mday).c_str());
        strcat(str999,"   ");
        strcat(str999,(char*)to_string(now->tm_hour).c_str());
        strcat(str999,":");
        strcat(str999,(char*)to_string(now->tm_min).c_str());
        strcat(str999,":");
        strcat(str999,(char*)to_string(now->tm_sec).c_str());
        strcat(str999,"]");
        strcat(str999,(char*)ui->lineEdit->text().toStdString().c_str());
        strcat(str999," Logged In");

        ofstream fout999("\\\\192.168.1.104\\guest\\SyntaX\\log.txt",ios::app);
        fout999<<str999<<"\n";
        fout999.close();

        //To Update the Online info
        ifstream fin1("\\\\192.168.1.104\\guest\\SyntaX\\online.txt", ios::in);

        for (int k = 0; k < 50; k++)
        {
            fin1.read((char*)&online[k], sizeof(online[k]));
        }

        fin1.close();

        for (int i = 0; i < 50; i++)
        {
            if (online[i][0] == '*')
            {
                strcpy(online[i], online_temp);
                online[i + 1][0] = '*';
                break;      //to break operation after updating the next line
            }
        }

        //Writing to the File (Entire Array containing all usernames)
        ofstream fout("\\\\192.168.1.104\\guest\\SyntaX\\online.txt",ios::out);
        for (int j = 0; j < 50; j++)
        {
            fout.write((char*)&online[j],sizeof(online[j]));
        }
        fout.close();

        //to set the label near logout to display the username

        ui->label->setText("Welcome " + ui->lineEdit->text());

        //to add to the list view

        ifstream fin2("\\\\192.168.1.104\\guest\\SyntaX\\online.txt", ios::in);
        //getting values to the array
        for (int k = 0; k < 50; k++)
        {
            fin2.read((char*)&online[k], sizeof(online[k]));
        }

        fin2.close();
        //extracting one string from the array of strings

        //QMessageBox::information(0,"Information","Flag1");

        for(int i = 0; i <50; i++)
        {
            //QMessageBox::information(0,"Information","Flag2");
            if(isalnum(online[i][0]) != 0 && isalnum(online[i][1]) != 0 && isalnum(online[i][2]) != 0)
            {
                //the string corresponding to the current value of i is a string starting with an alphanumeric char
                //QMessageBox::information(0,"Information","Flag3");
                //ui->stackedWidget->setCurrentIndex(1);
                ui->listWidget_2->addItem(online[i]);

            }
            //else
                //break;
        }

        timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(updateall()));
        timer->start(1000);

        this->setWindowTitle(ui->lineEdit->text());
        if(strcmp(ui->lineEdit->text().toStdString().c_str(),"Admin") == 0)
        {
            //Admin Login
            ui->stackedWidget->setCurrentIndex(2);
        }
        else
        {
            ui->stackedWidget->setCurrentIndex(1);
        }
    }
}

void MainWindow::on_pushButton_2_clicked()
{
    //Page 1 Sign Up Button

    MyClass mc;


        char str[100];

        int temp;
        temp = mc.validate((char*)ui->lineEdit->text().toStdString().c_str(),(char*)ui->lineEdit_2->text().toStdString().c_str(),0);

        if(temp == 0)
        {
            //Successful Signup
            mc.getinfo((char*)ui->lineEdit->text().toStdString().c_str(),(char*)ui->lineEdit_2->text().toStdString().c_str());
            QMessageBox::information(0,"Information","Login Now!");

            strcpy(str,"mkdir \\\\192.168.1.104\\guest\\SyntaX\\users\\");
            strcat(str,(char*)ui->lineEdit->text().toStdString().c_str());
            system(str);

        }
        if(temp == 1)
        {
            QMessageBox::warning(0,"Error","Username Already Exists!");

        }
}

void MainWindow::on_pushButton_3_clicked()
{
    //Send Button


    std::ofstream fout(chat_str,std::ios::app|std::ios::out);
    fout << ui->lineEdit->text().toStdString().c_str();
    fout << " : ";
    fout << ui->lineEdit_3->text().toStdString().c_str();
    fout << "\n";
    fout.close();

    std::ofstream fout1(chat_str2,std::ios::app|std::ios::out);
    fout1 << ui->lineEdit->text().toStdString().c_str();
    fout1 << " : ";
    fout1 << ui->lineEdit_3->text().toStdString().c_str();
    fout1 << "\n";
    fout1.close();

    ui->lineEdit_3->setText("");
}

void MainWindow::on_listWidget_2_itemClicked(QListWidgetItem *item)
{
    //ListWidgetItem clicked

    strcpy(chat_str,"\\\\192.168.1.104\\guest\\SyntaX\\users\\");
    strcat(chat_str,(char*)ui->lineEdit->text().toStdString().c_str());
    strcat(chat_str,"\\");
    strcat(chat_str,(char*)ui->listWidget_2->currentItem()->text().toStdString().c_str());
    strcat(chat_str,".txt");

    strcpy(chat_str2,"\\\\192.168.1.104\\guest\\SyntaX\\users\\");
    strcat(chat_str2,(char*)ui->listWidget_2->currentItem()->text().toStdString().c_str());
    strcat(chat_str2,"\\");
    strcat(chat_str2,(char*)ui->lineEdit->text().toStdString().c_str());
    strcat(chat_str2,".txt");

    item->setTextColor("Blue");
}

void MainWindow::on_pushButton_6_clicked()
{
    //Admin Logout

    ui->lineEdit_2->setText("");

    char str999[500];

    time_t t = time(0);   // get time now
    struct tm * now = localtime(&t);
    //cout << "[" << (now->tm_year + 1900) << '/'<< (now->tm_mon + 1) << '/'	<< now->tm_mday	<< "   " << now->tm_hour << ":" << now->tm_min << ":" << now->tm_sec << "]";

    strcpy(str999,"[");
    strcat(str999,(char*)to_string(now->tm_year + 1900).c_str());
    strcat(str999,"/");
    strcat(str999,(char*)to_string(now->tm_mon + 1).c_str());
    strcat(str999,"/");
    strcat(str999,(char*)to_string(now->tm_mday).c_str());
    strcat(str999,"   ");
    strcat(str999,(char*)to_string(now->tm_hour).c_str());
    strcat(str999,":");
    strcat(str999,(char*)to_string(now->tm_min).c_str());
    strcat(str999,":");
    strcat(str999,(char*)to_string(now->tm_sec).c_str());
    strcat(str999,"]");
    strcat(str999,(char*)ui->lineEdit->text().toStdString().c_str());
    strcat(str999," Logged Out");


    ofstream fout999("\\\\192.168.1.104\\guest\\SyntaX\\log.txt",ios::app);
    fout999<<str999<<"\n";
    fout999.close();

    //to delete the logged out username from the onine list

    char online_temp[25];
    strcpy(online_temp,(char*)ui->lineEdit->text().toStdString().c_str());

    char online[50][25];


    //updating array from file
    ifstream fin2("\\\\192.168.1.104\\guest\\SyntaX\\online.txt", ios::in);

    for (int l = 0; l < 50; l++)
    {
        fin2.read((char*)&online[l], sizeof(online[l]));

    }

    fin2.close();

    //updating finished

    for (int m = 0; m < 50; m++)
    {
        if (strcmp(online_temp, online[m]) == 0)
        {

            for (int n = m; n < 49; n++)
            {
                strcpy(online[n], online[n + 1]);

            }
            break;
        }
    }

    ofstream fout2("\\\\192.168.1.104\\guest\\SyntaX\\online.txt", ios::out);

    for (int o = 0; o < 50; o++)
    {
        fout2.write((char*)&online[o], sizeof(online[o]));
    }

    fout2.close();

    ui->listWidget_2->clear();

    ui->stackedWidget->setCurrentIndex(0);
    QMessageBox::information(0,"Information","Logout Successful!");
}

void MainWindow::on_pushButton_5_clicked()
{
    //Download Log
    system("copy \\\\192.168.1.104\\guest\\SyntaX\\log.txt c:\\SyntaX\\Downloads\\log.txt");
    QMessageBox::information(0,"Information","Download Complete under SyntaX downloads folder!");
}

void MainWindow::on_pushButton_7_clicked()
{
    //Download Chat
    char user[25];
    char chat[25];

    strcpy(user,(char*)ui->lineEdit_5->text().toStdString().c_str());
    strcpy(chat,(char*)ui->lineEdit_6->text().toStdString().c_str());

    char dir[250];
    char cmd[250];

    char name[51];

    strcpy(dir,"\\\\192.168.1.104\\guest\\SyntaX\\users\\");
    strcat(dir,user);
    strcat(dir,"\\");
    strcat(dir,chat);
    strcat(dir,".txt");

    strcpy(name,user);
    strcat(name,"-");
    strcat(name,chat);

    strcpy(cmd,"copy ");
    strcat(cmd,dir);
    strcat(cmd," c:\\SyntaX\\Downloads\\");
    strcat(cmd,name);
    strcat(cmd,".txt");

    system(cmd);
    QMessageBox::information(0,"Information","Download Complete under SyntaX downloads folder!");
}
